package edu.utexas.chow.wherewolf;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

public class GameActivity extends Activity {

	private List<Player> players = new ArrayList<Player>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game);
		populatePlayerList();
		populateListView();
	}

	private void populateListView(){
		ArrayAdapter<Player> adapter = new MyListAdapter();
		ListView list = (ListView) findViewById(R.id.game_players);
		list.setAdapter(adapter);
	}
	private class MyListAdapter extends ArrayAdapter<Player>{ 
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View itemView = convertView;
			
		//make sure to have a view to work with
			if (itemView == null){
				itemView = getLayoutInflater().inflate(R.layout.game_list, parent, false);
			}
			
			Player currentPlayer = players.get(position);
			ImageView imageView = (ImageView) itemView.findViewById(R.id.player_icon);
			imageView.setImageResource(currentPlayer.getimageUrl());
			
			TextView nameText = (TextView) itemView.findViewById(R.id.player_username);
			nameText.setText(currentPlayer.getUsername());
			return itemView;
		}

		public MyListAdapter(){
			super(GameActivity.this, R.layout.player_list, players);
		}
	}
	
	
	
	

	private void populatePlayerList() {
		players.add(new Player("kchow95", 1, R.drawable.link, 2));
		players.add(new Player("jyp", 2, R.drawable.kirby, 2));
		players.add(new Player("stace", 3, R.drawable.luigi, 2));
		players.add(new Player("alvindechipmonk", 4, R.drawable.mario, 1));
		
	}
}
