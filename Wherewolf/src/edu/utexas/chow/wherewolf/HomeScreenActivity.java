package edu.utexas.chow.wherewolf;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class HomeScreenActivity extends Activity {

	private static final String TAG = "homescreen";
	public final static String EXTRA_MESSAGE = "com.example.Wherewolf.HOMESCREEN";
	public void startCreateGame(){
		Intent intent = new Intent(this, CreateAGameActivity.class);
	    startActivity(intent);
	}
	public void createGame(View view){
			Toast.makeText(getApplicationContext(), "Redirecting...", 
		    Toast.LENGTH_SHORT).show();
			Log.i(TAG, "blah");
			startCreateGame();
			      
	}
	
	public void startGameSelection(){
		Intent intent = new Intent(this, GameSelectionActivity.class);
		Log.i(TAG, "switching screens");
	    startActivity(intent);
	}
	public void switchSelectGame(View view){
			Toast.makeText(getApplicationContext(), "Redirecting...", 
		    Toast.LENGTH_SHORT).show();
			Log.i(TAG, "hihihihi");
			startGameSelection();
			      
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_home_screen);
		final ImageButton button = (ImageButton) findViewById(R.id.create);
		Log.i(TAG, "screen appeared");
		View.OnClickListener jim = new View.OnClickListener() {
			public void onClick(View v) {
				createGame(v);
			}
		};
		button.setOnClickListener(jim);
		
		final ImageButton button2 = (ImageButton) findViewById(R.id.join);
		
		View.OnClickListener hal = new View.OnClickListener() {
			public void onClick(View v) {
				switchSelectGame(v);
			}
		};
		button2.setOnClickListener(hal);
	}
	@Override
	protected void onStart() {
		Log.i(TAG, "started the login activity");
		super.onStart();
	}

	@Override
	protected void onRestart() {
		Log.i(TAG, "restarted the login activity");
		super.onRestart();
	}

	@Override
	protected void onResume() {
		Log.i(TAG, "resumed the login activity");
		super.onResume();
	}

	@Override
	protected void onPause() {
		Log.i(TAG, "pause the login activity");
		super.onPause();
	}

	@Override
	protected void onStop() {
		Log.i(TAG, "stopped the login activity");
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		Log.i(TAG, "destroyed the login activity");
		super.onDestroy();
	}
}
