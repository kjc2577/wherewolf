package edu.utexas.chow.wherewolf;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class LoginActivity extends Activity {

	private static final String TAG = "loginactivity";
	public final static String EXTRA_MESSAGE = "com.example.Wherewolf.HOMESCREEN";
	private EditText username=null;
	private EditText password=null;
	
	public void startHomescreen(){
		Intent intent = new Intent(LoginActivity.this, HomeScreenActivity.class);
	    EditText editText = (EditText) findViewById(R.id.username);
	    String message = editText.getText().toString();
	    intent.putExtra(EXTRA_MESSAGE, message);
		Log.i(TAG, "logging in");
	    startActivity(intent);
	}
	
	public void login(View view){
		
			Toast.makeText(getApplicationContext(), "Redirecting...", 
		    Toast.LENGTH_SHORT).show();
			Log.i(TAG, "calling the logging in function");
			startHomescreen();	
			      
	}
	
	public void startRegister(){
		Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
		Log.i(TAG, "logging in");
	    startActivity(intent);
	}
	
	public void register(View view){
		
			Toast.makeText(getApplicationContext(), "Redirecting...", 
		    Toast.LENGTH_SHORT).show();
			Log.i(TAG, "calling the logging in function");
			startRegister();	
			      
	}
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_login);
	    username = (EditText)findViewById(R.id.username);
	    password = (EditText)findViewById(R.id.password);
	    final Button button = (Button) findViewById(R.id.loginButton);

		View.OnClickListener hal = new View.OnClickListener() {
			public void onClick(View v) {
				Log.i(TAG, "starting the login");
				login(v);
				
			}
		};
		button.setOnClickListener(hal);
		
		final Button button2 = (Button) findViewById(R.id.go_register);

		View.OnClickListener jordan = new View.OnClickListener() {
			public void onClick(View v) {
				Log.i(TAG, "starting the login");
				register(v);
				
			}
		};
		button2.setOnClickListener(jordan);

	}
	
	
	@Override
	protected void onStart() {
		Log.i(TAG, "started the login activity");
		super.onStart();
	}

	@Override
	protected void onRestart() {
		Log.i(TAG, "restarted the login activity");
		super.onRestart();
	}

	@Override
	protected void onResume() {
		Log.i(TAG, "resumed the login activity");
		super.onResume();
	}

	@Override
	protected void onPause() {
		Log.i(TAG, "pause the login activity");
		super.onPause();
	}

	@Override
	protected void onStop() {
		Log.i(TAG, "stopped the login activity");
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		Log.i(TAG, "destroyed the login activity");
		super.onDestroy();
	}
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.login, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//        if (id == R.id.action_settings) {
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }
}