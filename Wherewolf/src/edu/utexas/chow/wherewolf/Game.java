package edu.utexas.chow.wherewolf;

public class Game {
	private String gameName;
	private String description;
	private int gameId;
	private int hostIcon;
	
	public Game(String gameName, String description, int gameId, int hostIcon) {
		super();
		this.gameName = gameName;
		this.description = description;
		this.gameId = gameId;
		this.hostIcon = hostIcon;
	}
	
	public String getGameName() {
		return gameName;
	}
	public void setGameName(String gameName) {
		this.gameName = gameName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getGameId() {
		return gameId;
	}
	public void setGameId(int gameId) {
		this.gameId = gameId;
	}
	public int getHostIcon() {
		return hostIcon;
	}
	public void setHostIcon(int hostIcon) {
		this.hostIcon = hostIcon;
	}

	
	
	

}
