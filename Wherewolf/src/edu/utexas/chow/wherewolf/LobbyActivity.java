package edu.utexas.chow.wherewolf;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class LobbyActivity extends Activity {

	private List<Player> players = new ArrayList<Player>();
	public void startGame(){
		Intent intent = new Intent(LobbyActivity.this, GameActivity.class);
	    startActivity(intent);
	}
	
	public void switchToGame(View view){
		
			Toast.makeText(getApplicationContext(), "Redirecting...", 
		    Toast.LENGTH_SHORT).show();
			startGame();	
			      
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lobby);
		populatePlayerList();
		populateListView();
		final Button button = (Button) findViewById(R.id.start_game);

		View.OnClickListener hal = new View.OnClickListener() {
			public void onClick(View v) {
				switchToGame(v);
				
			}
		};
		button.setOnClickListener(hal);
	}
	
	private void populateListView(){
		ArrayAdapter<Player> adapter = new MyListAdapter();
		ListView list = (ListView) findViewById(R.id.list_players);
		list.setAdapter(adapter);
	}
	private class MyListAdapter extends ArrayAdapter<Player>{ 
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View itemView = convertView;
			
		//make sure to have a view to work with
			if (itemView == null){
				itemView = getLayoutInflater().inflate(R.layout.player_list, parent, false);
			}
			
			Player currentPlayer = players.get(position);
			ImageView imageView = (ImageView) itemView.findViewById(R.id.player_icon);
			imageView.setImageResource(currentPlayer.getimageUrl());
			
			TextView nameText = (TextView) itemView.findViewById(R.id.player_username);
			nameText.setText(currentPlayer.getUsername());
			return itemView;
		}

		public MyListAdapter(){
			super(LobbyActivity.this, R.layout.player_list, players);
		}
	}
	
	
	
	

	private void populatePlayerList() {
		players.add(new Player("kchow95", 1, R.drawable.link, 2));
		players.add(new Player("jyp", 2, R.drawable.kirby, 2));
		players.add(new Player("stace", 3, R.drawable.luigi, 2));
		players.add(new Player("alvindechipmonk", 4, R.drawable.mario, 1));
		
	}
	

}
