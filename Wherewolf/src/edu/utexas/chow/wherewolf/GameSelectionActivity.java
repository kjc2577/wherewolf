package edu.utexas.chow.wherewolf;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class GameSelectionActivity extends Activity {

	public void startLobby(){
		Intent intent = new Intent(GameSelectionActivity.this, LobbyActivity.class);
	    startActivity(intent);
	}
	
	public void join_lobby(View view){
		
			Toast.makeText(getApplicationContext(), "Redirecting...", 
		    Toast.LENGTH_SHORT).show();
			startLobby();	
			      
	}
	private List<Game> games = new ArrayList<Game>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game_selection);
		populateGameList();
		populateListView();
		 final Button button = (Button) findViewById(R.id.switch_lobby_button);

			View.OnClickListener hal = new View.OnClickListener() {
				public void onClick(View v) {
					join_lobby(v);
					
				}
			};
			button.setOnClickListener(hal);
	}
	
	private void populateListView(){
		ArrayAdapter<Game> adapter = new MyListAdapter();
		ListView list = (ListView) findViewById(R.id.game_list);
		list.setAdapter(adapter);
	}
	private class MyListAdapter extends ArrayAdapter<Game>{ 
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View itemView = convertView;
			
		//make sure to have a view to work with
			if (itemView == null){
				itemView = getLayoutInflater().inflate(R.layout.game_list_selection, parent, false);
			}
			
			Game currentGame = games.get(position);
			ImageView imageView = (ImageView) itemView.findViewById(R.id.game_host_icon);
			imageView.setImageResource(currentGame.getHostIcon());
			
			TextView nameText = (TextView) itemView.findViewById(R.id.game_name);
			nameText.setText(currentGame.getGameName());
			
			TextView descriptionText = (TextView) itemView.findViewById(R.id.game_description);
			descriptionText.setText(currentGame.getDescription());
			return itemView;
		}

		public MyListAdapter(){
			super(GameSelectionActivity.this, R.layout.game_list_selection, games);
		}
	}
	
	
	
	

	private void populateGameList() {
		games.add(new Game("NightHunt", "swag", 2, R.drawable.link));
		games.add(new Game("Lupus", "Fun fun", 2, R.drawable.mario));
		games.add(new Game("Freewins.com", "ahhhh", 2, R.drawable.kirby));
		
	}
}
