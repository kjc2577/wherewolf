package edu.utexas.chow.wherewolf;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class CreateAGameActivity extends Activity {

	private static final String TAG = "CreateGameActivity";
	public void startLobby()
	{
		Intent intent = new Intent(this, LobbyActivity.class);
		Log.i(TAG, "logging in");
	    startActivity(intent);
	}
	public void lobby(View view){
		
		Toast.makeText(getApplicationContext(), "Redirecting...", 
	    Toast.LENGTH_SHORT).show();
		Log.i(TAG, "calling the logging in function");
		startLobby();	
		      
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_agame);
		final Button button = (Button) findViewById(R.id.create_game_button);

		View.OnClickListener hal = new View.OnClickListener() {
			public void onClick(View v) {
				Log.i(TAG, "starting the login");
				lobby(v);
				
			}
		};
		button.setOnClickListener(hal);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.create_agame, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
