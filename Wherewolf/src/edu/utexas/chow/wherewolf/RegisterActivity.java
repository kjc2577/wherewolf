package edu.utexas.chow.wherewolf;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class RegisterActivity extends Activity {

	public void startLogin(){
		Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
	    startActivity(intent);
	}
	
	public void register(View view){
		
			Toast.makeText(getApplicationContext(), "Redirecting...", 
		    Toast.LENGTH_SHORT).show();
			startLogin();	
			      
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		final Button button = (Button) findViewById(R.id.register_button);

		View.OnClickListener hal = new View.OnClickListener() {
			public void onClick(View v) {
				register(v);
			}
		};
		button.setOnClickListener(hal);
	}

}
