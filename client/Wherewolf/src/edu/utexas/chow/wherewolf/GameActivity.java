package edu.utexas.chow.wherewolf;

import java.util.ArrayList;
import java.util.List;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class GameActivity extends Activity  {

	 private SeekBar barOpacity;
	 private ImageView image_moon;
	 private ImageView image_sun;
	 
	 
	 int counter = 0;
	 private static final String TAG = "updategame";
	private List<Player> players = new ArrayList<Player>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game);
		populatePlayerList();
		populateListView();
		image_moon = (ImageView)findViewById(R.id.moon);
		image_sun = (ImageView)findViewById(R.id.sun);
		barOpacity = (SeekBar)findViewById(R.id.opacity);
	
		final LocationManager locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
		Log.i(TAG, "created location manager");
		final LocationListener listener = new LocationListener(){

			@Override
			public void onLocationChanged(Location location) {
				 Log.i(TAG, "onLocationChanged called");
			        if (location != null) {
			        	WherewolfPreferences user = new WherewolfPreferences(GameActivity.this);
			        	Log.i(TAG, "hello");
			        	double lat = location.getLatitude();
			        	double lng = location.getLongitude();
			        	
			        	String stringLat = String.valueOf(lat);
			        	String stringLng = String.valueOf(lng);
			        	//Change this later
			        	int gameID = user.getCurrentGameID();
			        	String stringGameID = String.valueOf(gameID);
			        	String username = user.getUsername();
			        	String password = user.getPassword();
			        	
			            final String locMsg = "location changed "+ lat + " "+ lng;

			            Toast.makeText(getBaseContext(), locMsg, Toast.LENGTH_SHORT).show();
			            //showLocation(locMsg);

			            Log.i(TAG, locMsg);
			            UpdateGameRequest update = new UpdateGameRequest(username, password, stringLat, stringLng, "1");
			           
			            new GameActivityAsyncTask().execute(update);
			            Log.i(TAG, "made it");
			        }
			}

			@Override
			public void onStatusChanged(String provider, int status,
					Bundle extras) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onProviderEnabled(String provider) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onProviderDisabled(String provider) {
				
			}
			
		};
		Log.i(TAG, "calling request locaiton updates");
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 500, 0, listener);
		Log.i(TAG, "didn't crash");
		int alpha = barOpacity.getProgress();
		
		image_moon.setAlpha(alpha);   //deprecated
		image_sun.setAlpha(alpha);   //deprecated
		
		barOpacity.setOnSeekBarChangeListener(barOpacityOnSeekBarChangeListener);
		final Button button = (Button) findViewById(R.id.leave_game_button);

		View.OnClickListener hal = new View.OnClickListener() {
			public void onClick(View v) {
				Log.i(TAG, "Leave game clicked");
				WherewolfPreferences user = new WherewolfPreferences(GameActivity.this);
				String username = user.getUsername();
				String password = user.getPassword();
				String gameId = Integer.toString(user.getCurrentGameID());
				LeaveGameRequest request = new LeaveGameRequest(username, password, "1");
				locationManager.removeUpdates(listener);
				new LeaveGameAsyncTask().execute(request);
			}
		};
		button.setOnClickListener(hal);
	}
	
	
	 OnSeekBarChangeListener barOpacityOnSeekBarChangeListener =
			  new OnSeekBarChangeListener(){

		
			   @Override
			   public void onProgressChanged(SeekBar seekBar, int progress,
			     boolean fromUser) {
			    int alpha = barOpacity.getProgress();
			    //image.setImageAlpha(alpha); //for API Level 16+
			  
				 barOpacity.setOnSeekBarChangeListener(barOpacityOnSeekBarChangeListener);
				    if (alpha < 50){
				    	image_moon.setAlpha(200-alpha*4);
				    	image_sun.setAlpha(0);
				    	
				    }else{
				    	image_moon.setAlpha(0);
				    	image_sun.setAlpha((alpha-50)*4);
				    }
				 
				    
			   }
			   @Override
			   public void onStartTrackingTouch(SeekBar seekBar) {}

			   @Override
			   public void onStopTrackingTouch(SeekBar seekBar) {}
			   
			 };


	private void populateListView(){
		ArrayAdapter<Player> adapter = new MyListAdapter();
		ListView list = (ListView) findViewById(R.id.game_players);
		list.setAdapter(adapter);
	}
	private class GameActivityAsyncTask extends AsyncTask<UpdateGameRequest, Integer, UpdateGameResponse> {
	    @Override
	    protected UpdateGameResponse doInBackground(UpdateGameRequest... request) {
	    	Log.i(TAG, request[0].getURL() + request[0].getUsername());
	        return request[0].execute(new WherewolfNetworking());
	    }

	    protected void onPostExecute(UpdateGameResponse result) {
	    	Log.i(TAG, "weeeeeeee");
	        
	        
	        if (result.getStatus().equals("success")) {
	            
	        	String time = result.getCurrentTime();
	        	int iTime = Integer.valueOf(time.substring(0, 2));
	        	
	        	Log.i(TAG, Integer.toString(iTime));
	        	if (iTime >= 19 && iTime <=24 || iTime >= 0 && iTime <7)
	        	{
	        		barOpacity.setProgress(0);
	        	}
	        	else 
	        	{
	        		barOpacity.setProgress(200);
	        	}
	            Log.i(TAG, "it worked");
	           
	      //      overridePendingTransition(R.anim.slide_in_right,
	        //            R.anim.slide_out_left);
	        } 
	        else {
	        	Log.i(TAG, "it  didn't worked");
	        }
	    	}
		}
	private class LeaveGameAsyncTask extends AsyncTask<LeaveGameRequest, Integer, LeaveGameResponse> {
	    @Override
	    protected LeaveGameResponse doInBackground(LeaveGameRequest... request) {
	    	Log.i(TAG, request[0].getURL() + request[0].getUsername());
	        return request[0].execute(new WherewolfNetworking());
	    }

	    protected void onPostExecute(LeaveGameResponse result) {
	    	Log.i(TAG, "weeeeeeee");
	        
	        if (result.getStatus().equals("success")) {
	        	Intent intent = new Intent(GameActivity.this, GameSelectionActivity.class);
	        	startActivity(intent);
	        }
		}
	}
	
	
	private class MyListAdapter extends ArrayAdapter<Player>{ 
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View itemView = convertView;
			
		//make sure to have a view to work with
			if (itemView == null){
				itemView = getLayoutInflater().inflate(R.layout.game_list, parent, false);
			}
			
			Player currentPlayer = players.get(position);
			ImageView imageView = (ImageView) itemView.findViewById(R.id.player_icon);
			imageView.setImageResource(currentPlayer.getimageUrl());
			
			TextView nameText = (TextView) itemView.findViewById(R.id.player_username);
			nameText.setText(currentPlayer.getUsername());
			return itemView;
		}

		public MyListAdapter(){
			super(GameActivity.this, R.layout.player_list, players);
		}
	}

	private void populatePlayerList() {
		players.add(new Player("kchow95", 1, R.drawable.link, 2));
		players.add(new Player("jyp", 2, R.drawable.kirby, 2));
		players.add(new Player("stace", 3, R.drawable.luigi, 2));
		players.add(new Player("alvindechipmonk", 4, R.drawable.mario, 1));
		
	}
	
	@Override
	protected void onStart() {
		Log.i(TAG, "started the login activity");
		super.onStart();
	}

	@Override
	protected void onRestart() {
		Log.i(TAG, "restarted the login activity");
		super.onRestart();
	}

	@Override
	protected void onResume() {
		Log.i(TAG, "resumed the login activity");
		super.onResume();
	}

	@Override
	protected void onPause() {
		Log.i(TAG, "pause the login activity");
		super.onPause();
	}

	@Override
	protected void onStop() {
		Log.i(TAG, "stopped the login activity");
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		Log.i(TAG, "destroyed the login activity");
		super.onDestroy();
	}

}
