package edu.utexas.chow.wherewolf;

public class UpdateGameResponse extends BasicResponse{
	 
	  private final String currentTime;
	  public UpdateGameResponse(String status, String errorMessage) {
	      super(status, errorMessage);
	      currentTime = "";
	  }
	  
	  public UpdateGameResponse(String status, String errorMessage, String currentTime) {
	      super(status, errorMessage);
	      this.currentTime = currentTime;
	      
	  }
	  
	  public String getCurrentTime() {
		return currentTime;
	}


	
}
