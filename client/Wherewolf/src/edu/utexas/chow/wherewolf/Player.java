package edu.utexas.chow.wherewolf;

public class Player {
	private String username;
	private int playerId;
	private int imageUrl;
	private int numVotes;
	
	
	
	public Player (String username, int playerId, int imageUrl, int numVotes){
		this.username = username;
		this.playerId = playerId;
		this.imageUrl = imageUrl;
		this.numVotes = numVotes;
	}
	
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public int getPlayerId() {
		return playerId;
	}
	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}
	public int getimageUrl() {
		return imageUrl;
	}
	public void setimageUrl(int imageUrl) {
		this.imageUrl = imageUrl;
	}
	public int getNumVotes() {
		return numVotes;
	}
	public void setNumVotes(int numVotes) {
		this.numVotes = numVotes;
	}
	
	

}
