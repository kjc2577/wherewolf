package edu.utexas.chow.wherewolf;

public class LeaveGameResponse extends BasicResponse {

	  
	  private int playerID = -1;

	  public LeaveGameResponse(String status, String errorMessage) {
	      super(status, errorMessage);
	  }
	  
	  public LeaveGameResponse(String status, String errorMessage, int playerID) {
	      super(status, errorMessage);
	      
	      this.playerID = playerID;
	  }

	  
	  public int getPlayerID()
	  {
	      return playerID;
	  }
}
