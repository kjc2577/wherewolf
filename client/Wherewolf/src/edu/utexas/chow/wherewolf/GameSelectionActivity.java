package edu.utexas.chow.wherewolf;

import java.util.ArrayList;
import java.util.List;



import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;

import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class GameSelectionActivity extends Activity {

	private static final String TAG = "gameselection";
	private List<Game> games = new ArrayList<Game>();
	private String gameName = "";
	private String gameDescription = "";
	private void getNewGame(){
        WherewolfPreferences pref = new WherewolfPreferences(GameSelectionActivity.this);
        gameName = pref.getGameName();
        gameDescription = pref.getGameDescription();
	}
	public void startLobby(){
		Intent intent = new Intent(GameSelectionActivity.this, LobbyActivity.class);
	    startActivity(intent);
	}
	
	public void join_lobby(View view){
		
			Toast.makeText(getApplicationContext(), "Redirecting...", 
		    Toast.LENGTH_SHORT).show();
			startLobby();	
			      
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game_selection);
		getNewGame();
		populateGameList();
		populateListView();
		 final Button button = (Button) findViewById(R.id.switch_lobby_button);

			View.OnClickListener hal = new View.OnClickListener() {
				public void onClick(View v) {
					WherewolfPreferences pref = new WherewolfPreferences(GameSelectionActivity.this);
					String username = pref.getUsername();
					String password = pref.getPassword();
			        Log.i(TAG, "calling async task");
					JoinGameRequest request = new JoinGameRequest(username, password, 1);	
					new JoinGameAsyncTask().execute(request);
					
				}
			};
			button.setOnClickListener(hal);
			

			final Button button2 = (Button) findViewById(R.id.create_game_button);
			Log.i(TAG, "screen appeared");
			View.OnClickListener jim = new View.OnClickListener() {
				public void onClick(View v) {
					Intent intent = new Intent(GameSelectionActivity.this, CreateAGameActivity.class);
					startActivity(intent);
				}
			};
			button2.setOnClickListener(jim);
			final Button button3 = (Button) findViewById(R.id.log_out_button);
			View.OnClickListener pop = new View.OnClickListener() {
				public void onClick(View v) {
					Log.i(TAG, "Log out clicked");
					WherewolfPreferences pref = new WherewolfPreferences(GameSelectionActivity.this);
					pref.clear();
					Intent intent = new Intent(GameSelectionActivity.this, LoginActivity.class);
					startActivity(intent);
				}
			};
			button3.setOnClickListener(pop);
	}
	
	
	private void populateListView(){
		ArrayAdapter<Game> adapter = new MyListAdapter();
		ListView list = (ListView) findViewById(R.id.game_list);
		list.setAdapter(adapter);
	}
	private class MyListAdapter extends ArrayAdapter<Game>{ 
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View itemView = convertView;
			
		//make sure to have a view to work with
			if (itemView == null){
				itemView = getLayoutInflater().inflate(R.layout.game_list_selection, parent, false);
			}
			
			Game currentGame = games.get(position);
			ImageView imageView = (ImageView) itemView.findViewById(R.id.game_host_icon);
			imageView.setImageResource(currentGame.getHostIcon());
			
			TextView nameText = (TextView) itemView.findViewById(R.id.game_name);
			nameText.setText(currentGame.getGameName());
			
			TextView descriptionText = (TextView) itemView.findViewById(R.id.game_description);
			descriptionText.setText(currentGame.getDescription());
			return itemView;
		}

		public MyListAdapter(){
			super(GameSelectionActivity.this, R.layout.game_list_selection, games);
		}
	}
	private class JoinGameAsyncTask extends AsyncTask<JoinGameRequest, Integer, JoinGameResponse> {

	      @Override
	      protected JoinGameResponse doInBackground(JoinGameRequest... request) {
	    	  Log.v(TAG, "Starting the doinbackground");

	    	  //get value of game_id from listview**
	    	  Log.i(TAG, "Inside async task");
	          WherewolfPreferences pref = new WherewolfPreferences(GameSelectionActivity.this);
	          String username = pref.getUsername();
	          String password = pref.getPassword();
	          
	          JoinGameRequest joinGameRequest = new JoinGameRequest(username, password, 1);
	          return joinGameRequest.execute(new WherewolfNetworking());
  
	      }

	      protected void onPostExecute(JoinGameResponse result) {
	    	  Log.v(TAG, "Starting the onpostexecute");
	          //final TextView errorText = (TextView) findViewById(R.id.error_text);
	          
	          //if (result.getStatus().equals("success")) {
	    	  if (true){
	    		  Log.i(TAG,  "it worked");
	              WherewolfPreferences pref = new WherewolfPreferences(GameSelectionActivity.this);
	              pref.setCurrentGameID(result.getGameID());

	              //errorText.setText("");
	              
	              //go to next screen
	              Log.v(TAG, "Creating Game");
	      		  Intent startCreateGameIntent = new Intent(GameSelectionActivity.this, LobbyActivity.class);
	    		  startActivity(startCreateGameIntent);
	    		  
	    		  //animation
	              //overridePendingTransition(R.anim.slide_in_right,
	              //        R.anim.slide_out_left);
	          } else {
	              // do something with bad password
	              
	              //errorText.setText(result.getErrorMessage());
	          }
	    	  Log.v(TAG, "Finishing the onpostexecute");
	      }

	  }
	
	
	

	private void populateGameList() {
		games.add(new Game("NightHunt", "swag", 2, R.drawable.link));
		games.add(new Game("Lupus", "Fun fun", 2, R.drawable.mario));
		games.add(new Game("Freewins.com", "ahhhh", 2, R.drawable.kirby));
		
	}
}
