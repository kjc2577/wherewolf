package edu.utexas.chow.wherewolf;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Provides an abstraction for storing the shared preferences
 * @author rfdickerson
 *
 */
public class WherewolfPreferences {

  private static final String PREF_URI = "edu.utexas.werewolf.prefs";
  
  Context context;
  private SharedPreferences sharedPreferences;
  
  public WherewolfPreferences(Context context)
  {
      this.context = context;
      sharedPreferences = context
              .getSharedPreferences(PREF_URI,
                      Context.MODE_PRIVATE);
      
  }
  
  public String getUsername()
  {
      return sharedPreferences.getString("username", "");
  }
  public void setNewGame(String gameName, String gameDescription)
  {
      SharedPreferences.Editor editor = sharedPreferences.edit();
      
      editor.putString("gamename", gameName);
      editor.putString("gamedescription", gameDescription);
      editor.commit();
  }

  public String getGameName()
  {
      return sharedPreferences.getString("gamename", "");
  }
  
  public String getGameDescription()
  {
      return sharedPreferences.getString("gamedescription", "");
  }
  
  public String getPassword()
  {
      return sharedPreferences.getString("password", "");
  }
  
  public int getCurrentGameID()
  {
      return sharedPreferences.getInt("currentGame", 0);
  }
  
  public void setCreds(String username, String password)
  {
      SharedPreferences.Editor editor = sharedPreferences.edit();
      
      editor.putString("username", username);
      editor.putString("password", password);
      editor.commit();
  }
  
  public void setCurrentGameID(int gameID)
  {
      SharedPreferences.Editor editor = sharedPreferences.edit();
      editor.putInt("currentGame", gameID);
      editor.commit();
  }
  public long getTime()
  {
	  return sharedPreferences.getLong("time", 0);
	  
  }
  public void setTime(long time)
  {
	  SharedPreferences.Editor editor = sharedPreferences.edit();
	  editor.putLong("time", time);
	  editor.commit();
  }
  public void clear()
  {
	  SharedPreferences.Editor editor = sharedPreferences.edit();
	  editor.clear();
	  editor.commit();
  }
  
}