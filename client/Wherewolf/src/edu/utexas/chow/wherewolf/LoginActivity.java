package edu.utexas.chow.wherewolf;


import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class LoginActivity extends Activity {

	private static final String TAG = "loginactivity";
	public final static String EXTRA_MESSAGE = "com.example.Wherewolf.HOMESCREEN";
	
	public void startHomescreen(){
		Intent intent = new Intent(LoginActivity.this, HomeScreenActivity.class);
	    EditText editText = (EditText) findViewById(R.id.username);
	    String message = editText.getText().toString();
	    intent.putExtra(EXTRA_MESSAGE, message);
		Log.i(TAG, "logging in");
	    startActivity(intent);
	}
	
	public void login(View view){
		
			Toast.makeText(getApplicationContext(), "Redirecting...", 
		    Toast.LENGTH_SHORT).show();
			Log.i(TAG, "calling the logging in function");
			startHomescreen();	
			      
	}
	
	public void startRegister(){
		Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
		Log.i(TAG, "logging in");
	    startActivity(intent);
	}
	
	public void register(View view){
		
			Toast.makeText(getApplicationContext(), "Redirecting...", 
		    Toast.LENGTH_SHORT).show();
			Log.i(TAG, "calling the logging in function");
			startRegister();	
			      
	}
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_login);
	    final Button button = (Button) findViewById(R.id.loginButton);
	    
	  

		View.OnClickListener hal = new View.OnClickListener() {
			public void onClick(View v) {
				Log.i(TAG, "starting the login");
				final EditText nameTV = (EditText) findViewById(R.id.username);
			     final EditText passTV = (EditText) findViewById(R.id.password);
			        
			     String username = nameTV.getText().toString();
			     String password = passTV.getText().toString();
			     
			     SigninRequest request = new SigninRequest(username, password);
			     new SigninTask().execute(request);
			    
				
				
			}
		};
		button.setOnClickListener(hal);
		
		final Button button2 = (Button) findViewById(R.id.go_register);

		View.OnClickListener jordan = new View.OnClickListener() {
			public void onClick(View v) {
				Log.i(TAG, "starting the login");
				register(v);
				
			}
		};
		button2.setOnClickListener(jordan);

	}
	public static String md5(String string) {
	    byte[] hash;

	    try {
	        hash = MessageDigest.getInstance("MD5").digest(string.getBytes("UTF-8"));
	    } catch (NoSuchAlgorithmException e) {
	        throw new RuntimeException("Huh, MD5 should be supported?", e);
	    } catch (UnsupportedEncodingException e) {
	        throw new RuntimeException("Huh, UTF-8 should be supported?", e);
	    }

	    StringBuilder hex = new StringBuilder(hash.length * 2);

	    for (byte b : hash) {
	        int i = (b & 0xFF);
	        if (i < 0x10) hex.append('0');
	        hex.append(Integer.toHexString(i));
	    }

	    return hex.toString();
	}
	
	private class SigninTask extends AsyncTask<SigninRequest, Integer, SigninResponse> {
    @Override
    protected SigninResponse doInBackground(SigninRequest... request) {

    	final EditText nameTV = (EditText) findViewById(R.id.username);
        final EditText passTV = (EditText) findViewById(R.id.password);
        
        String username = nameTV.getText().toString();
        String password = passTV.getText().toString();
        String hashPass = md5(password);
        
        Log.i(TAG, hashPass);
        SigninRequest signinRequest = new SigninRequest(username, hashPass);
        
        return signinRequest.execute(new WherewolfNetworking());
    }

    protected void onPostExecute(SigninResponse result) {
    	Log.i(TAG, "weeeeeeee");
        final TextView errorText = (TextView) findViewById(R.id.error_text);
        
        if (result.getStatus().equals("success")) {
            
            final EditText nameTV = (EditText) findViewById(R.id.username);
            final EditText passTV = (EditText) findViewById(R.id.password);
            String hashPass = md5(passTV.getText().toString());
            WherewolfPreferences pref = new WherewolfPreferences(LoginActivity.this);
            pref.setCreds(nameTV.getText().toString(), hashPass);

            errorText.setText("");
            Log.v(TAG, "Signing in");
            Intent intent = new Intent(LoginActivity.this, GameSelectionActivity.class);
            startActivity(intent);
      //      overridePendingTransition(R.anim.slide_in_right,
        //            R.anim.slide_out_left);
        } else {
            // do something with bad password
            
            errorText.setText(result.getMessage());
        }

    	}
	}
	
	
	@Override
	protected void onStart() {
		Log.i(TAG, "started the login activity");
		super.onStart();
	}

	@Override
	protected void onRestart() {
		Log.i(TAG, "restarted the login activity");
		super.onRestart();
	}

	@Override
	protected void onResume() {
		Log.i(TAG, "resumed the login activity");
		super.onResume();
	}

	@Override
	protected void onPause() {
		Log.i(TAG, "pause the login activity");
		super.onPause();
	}

	@Override
	protected void onStop() {
		Log.i(TAG, "stopped the login activity");
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		Log.i(TAG, "destroyed the login activity");
		super.onDestroy();
	}
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.login, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//        if (id == R.id.action_settings) {
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }
}