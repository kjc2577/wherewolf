package edu.utexas.chow.wherewolf;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class RegisterRequest extends BasicRequest {
	
	private static final String TAG = "registeractivity";
	private String firstName;
	private	String lastName;
	public RegisterRequest(String username, String password, String firstName, String lastName){
		super(username, password);
		this.firstName = firstName;
		this.lastName = lastName;
	}

	 @Override
	  public String getURL() {
	      return "/register";
	  }

	  @Override
	  public List<NameValuePair> getParameters() {
		  List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
	      urlParameters.add(new BasicNameValuePair("firstname", firstName));
	      urlParameters.add(new BasicNameValuePair("lastname", lastName));
	      return urlParameters;
	  }

	  @Override
	  public RequestType getRequestType() {
	      return RequestType.POST;
	  }

	  @Override
	  public RegisterResponse execute(WherewolfNetworking net) {
	  
	      try {
	          JSONObject response = net.sendRequest(this);
	          Log.i(TAG, response.getString("status"));
	          
	          if (response.getString("status").equals("success"))
	          {
	        	  Log.i(TAG, "success");
	              // int playerID = response.getInt("playerid");
	              return new RegisterResponse("success", "signed in successfully");
	          } else {
	        	  Log.i(TAG, "failure");
	              String errorMessage = response.getString("error");
	              return new RegisterResponse("failure", errorMessage);
	          }
	      } catch (JSONException e) {
	    	  Log.i(TAG, "crap");
	          return new RegisterResponse("failure", "sign in not working");
	      } catch (WherewolfNetworkException ex)
	      {
	    	  Log.i(TAG, "oh no");
	          return new RegisterResponse("failure", "could not communicate with the server");
	      }
	 }

}


