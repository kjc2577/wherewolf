package edu.utexas.chow.wherewolf;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class LeaveGameRequest extends BasicRequest {

	private static final String TAG = "registeractivity";
	private String gameid;
	public LeaveGameRequest(String username, String password, String gameid){
		super(username, password);
		this.gameid = gameid;
	}

	 @Override
	  public String getURL() {
	      return "/v1/game/"+Integer.parseInt(gameid)+"/leave";
	  }

	  @Override
	  public List<NameValuePair> getParameters() {
		  List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
	      urlParameters.add(new BasicNameValuePair("game_id", gameid));
	      return urlParameters;
	  }

	  @Override
	  public RequestType getRequestType() {
	      return RequestType.POST;
	  }

	  @Override
	  public LeaveGameResponse execute(WherewolfNetworking net) {
	  
	      try {
	          JSONObject response = net.sendRequest(this);
	          Log.i(TAG, response.getString("status"));
	          
	          if (response.getString("status").equals("success"))
	          {
	        	  Log.i(TAG, "success");
	              // int playerID = response.getInt("playerid");
	              return new LeaveGameResponse("success", "left game succesfully");
	          } else {
	        	  Log.i(TAG, "failure");
	              String errorMessage = response.getString("error");
	              return new LeaveGameResponse("failure", errorMessage);
	          }
	      } catch (JSONException e) {
	    	  Log.i(TAG, "crap");
	          return new LeaveGameResponse("failure", "sign in not working");
	      } catch (WherewolfNetworkException ex)
	      {
	    	  Log.i(TAG, "oh no");
	          return new LeaveGameResponse("failure", "could not communicate with the server");
	      }
	 }
}
