package edu.utexas.chow.wherewolf;

import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;
import edu.utexas.chow.wherewolf.BasicRequest.RequestType;

public class GameSelectionRequest extends BasicRequest {

	private static final String TAG = "gameselection";
	public GameSelectionRequest(String username, String password){
		super(username, password);
	}

	 @Override
	  public String getURL() {
	      return "/v1/check_password";
	  }

	  @Override
	  public List<NameValuePair> getParameters() {
	      return null;
	  }

	  @Override
	  public RequestType getRequestType() {
	      return RequestType.GET;
	  }

	  @Override
	  public GameSelectionResponse execute(WherewolfNetworking net) {
	  
	      try {
	          JSONObject response = net.sendRequest(this);
	          Log.i(TAG, response.getString("status"));
	          
	          if (response.getString("status").equals("success"))
	          {
	        	  Log.i(TAG, "success");
	              // int playerID = response.getInt("playerid");
	              return new GameSelectionResponse("success", "signed in successfully");
	          } else {
	        	  Log.i(TAG, "failure");
	              String errorMessage = response.getString("error");
	              return new GameSelectionResponse("failure", errorMessage);
	          }
	      } catch (JSONException e) {
	    	  Log.i(TAG, "crap");
	          return new GameSelectionResponse("failure", "sign in not working");
	      } catch (WherewolfNetworkException ex)
	      {
	    	  Log.i(TAG, "oh no");
	          return new GameSelectionResponse("failure", "could not communicate with the server");
	      }
	 }

}
