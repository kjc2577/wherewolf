package edu.utexas.chow.wherewolf;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RegisterActivity extends Activity {
	
	private static final String TAG = "registeractivity";

	public void startLogin(){
		Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
	    startActivity(intent);
	}
	
	public void register(View view){
		
			Toast.makeText(getApplicationContext(), "Redirecting...", 
		    Toast.LENGTH_SHORT).show();
			startLogin();	
			      
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		final Button button = (Button) findViewById(R.id.register_button);

		View.OnClickListener hal = new View.OnClickListener() {
			public void onClick(View v) {
				Log.i(TAG, "starting the login");
				final EditText nameTV = (EditText) findViewById(R.id.username_register);
			    final EditText passTV = (EditText) findViewById(R.id.password_register);
			    final EditText firstTV = (EditText) findViewById(R.id.first_name);
			    final EditText lastTV = (EditText) findViewById(R.id.last_name);
			        
			     String username = nameTV.getText().toString();
			     String password = passTV.getText().toString();
			     String firstName = firstTV.getText().toString();
			     String lastName = lastTV.getText().toString();
			     
			     RegisterRequest request = new RegisterRequest(username, password, firstName, lastName);
			     new RegisterAsyncTask().execute(request);
			    
			}
		};
		button.setOnClickListener(hal);
	}
	
	public static String md5(String string) {
	    byte[] hash;

	    try {
	        hash = MessageDigest.getInstance("MD5").digest(string.getBytes("UTF-8"));
	    } catch (NoSuchAlgorithmException e) {
	        throw new RuntimeException("Huh, MD5 should be supported?", e);
	    } catch (UnsupportedEncodingException e) {
	        throw new RuntimeException("Huh, UTF-8 should be supported?", e);
	    }

	    StringBuilder hex = new StringBuilder(hash.length * 2);

	    for (byte b : hash) {
	        int i = (b & 0xFF);
	        if (i < 0x10) hex.append('0');
	        hex.append(Integer.toHexString(i));
	    }

	    return hex.toString();
	}
	
	private class RegisterAsyncTask extends AsyncTask<RegisterRequest, Integer, RegisterResponse> {
	    @Override
	    protected RegisterResponse doInBackground(RegisterRequest... request) {

	    	final EditText nameTV = (EditText) findViewById(R.id.username_register);
	        final EditText passTV = (EditText) findViewById(R.id.password_register);
	        //make a firstname and lastname box
	        final EditText firstTV = (EditText) findViewById(R.id.first_name);
	        final EditText lastTV = (EditText) findViewById(R.id.last_name);
	        String username = nameTV.getText().toString();
	        String password = passTV.getText().toString();
	        String firstName = firstTV.getText().toString();
	        String lastName = lastTV.getText().toString();
	        String hashPass = md5(password);
	        
	        Log.i(TAG, hashPass);
	        RegisterRequest registerRequest = new RegisterRequest(username, hashPass, firstName, lastName);
	        
	        return registerRequest.execute(new WherewolfNetworking());
	    }
	    protected void onPostExecute(RegisterResponse result) {
	    	Log.i(TAG, "weeeeeeee");
	       
	        
	        if (result.getStatus().equals("success")) {
	            
	            final EditText nameTV = (EditText) findViewById(R.id.username_register);
	            final EditText passTV = (EditText) findViewById(R.id.password_register);
	            String hashPass = md5(passTV.getText().toString());
	            
	            WherewolfPreferences pref = new WherewolfPreferences(RegisterActivity.this);
	            pref.setCreds(nameTV.getText().toString(), hashPass);
	            Log.v(TAG, "Signing in");
	            Intent intent = new Intent(RegisterActivity.this, GameSelectionActivity.class);
	            startActivity(intent);
	      //      overridePendingTransition(R.anim.slide_in_right,
	        //            R.anim.slide_out_left);
	        }
	    	}
		}
		

}
