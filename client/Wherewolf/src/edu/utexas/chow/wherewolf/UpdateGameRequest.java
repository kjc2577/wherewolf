package edu.utexas.chow.wherewolf;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;
import edu.utexas.chow.wherewolf.BasicRequest.RequestType;

public class UpdateGameRequest extends BasicRequest {
	
	private String lat;
	private String lng;
	private WherewolfService service;
	private String gameID;
	
	private static final String TAG = "updategame";
	public UpdateGameRequest(String username, String password, String lat, String lng, String gameID){
		super(username, password);
		this.lat = lat;
		this.lng = lng;
		this.gameID = gameID;
	}

	 @Override
	  public String getURL() {
		 Log.i(TAG, gameID);
	      return "/v1/game/"+Integer.parseInt(gameID);
	  }

	  @Override
	  public List<NameValuePair> getParameters() {
		  List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
	      urlParameters.add(new BasicNameValuePair("lat", lat));
	      Log.i(TAG, lat);
	      urlParameters.add(new BasicNameValuePair("lng", lng));
	      urlParameters.add(new BasicNameValuePair("game_id", gameID));
	      
	      return urlParameters;
	  }

	  @Override
	  public RequestType getRequestType() {
	      return RequestType.POST;
	  }

	  @Override
	  public UpdateGameResponse execute(WherewolfNetworking net) {
	  
	      try {
	          JSONObject response = net.sendRequest(this);
	          Log.i(TAG, response.getString("status"));
	          
	          if (response.getString("status").equals("success"))
	          {
	        	  Log.i(TAG, "success");
	              // int playerID = response.getInt("playerid");
		          Log.i(TAG, response.getString("results"));
	              return new UpdateGameResponse("success", "signed in successfully", response.getString("results"));
	          } else {
	        	  Log.i(TAG, "failure");
	              String errorMessage = response.getString("error");
	              return new UpdateGameResponse("failure", errorMessage);
	          }
	      } catch (JSONException e) {
	    	  Log.i(TAG, "crap");
	          return new UpdateGameResponse("failure", "sign in not working");
	      } catch (WherewolfNetworkException ex)
	      {
	    	  Log.i(TAG, "oh no");
	          return new UpdateGameResponse("failure", "could not communicate with the server");
	      }
	 }

}
