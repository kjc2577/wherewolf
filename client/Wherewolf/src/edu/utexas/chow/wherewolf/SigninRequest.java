package edu.utexas.chow.wherewolf;

import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class SigninRequest extends BasicRequest {
	
	private static final String TAG = "loginactivity";
	public SigninRequest(String username, String password){
		super(username, password);
	}

	 @Override
	  public String getURL() {
	      return "/v1/check_password";
	  }

	  @Override
	  public List<NameValuePair> getParameters() {
	      return null;
	  }

	  @Override
	  public RequestType getRequestType() {
	      return RequestType.GET;
	  }

	  @Override
	  public SigninResponse execute(WherewolfNetworking net) {
	  
	      try {
	          JSONObject response = net.sendRequest(this);
	          Log.i(TAG, response.getString("status"));
	          
	          if (response.getString("status").equals("success"))
	          {
	        	  Log.i(TAG, "success");
	              // int playerID = response.getInt("playerid");
	              return new SigninResponse("success", "signed in successfully");
	          } else {
	        	  Log.i(TAG, "failure");
	              String errorMessage = response.getString("error");
	              return new SigninResponse("failure", errorMessage);
	          }
	      } catch (JSONException e) {
	    	  Log.i(TAG, "crap");
	          return new SigninResponse("failure", "sign in not working");
	      } catch (WherewolfNetworkException ex)
	      {
	    	  Log.i(TAG, "oh no");
	          return new SigninResponse("failure", "could not communicate with the server");
	      }
	 }

}


