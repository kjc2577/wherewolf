package edu.utexas.chow.wherewolf;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.Process;
import android.util.Log;
import android.widget.Toast;
public class WherewolfService extends Service implements LocationListener {

	  // number of milliseconds before a location update
	  private static final int REPORT_PERIOD = 5000;
	  private static final int MIN_CHANGE = 0;
	  private static final String TAG = "updategame";
	  private WherewolfPreferences user;
	  // allows us to prevent the CPU from going to sleep.
	  private WakeLock wakeLock;

	  // allows us to register updates to the GPS system
	  private LocationManager locationManager;

	  

	  private boolean isNight = false;
	  private Game currentGame = null;
	  private Player currentPlayer = null;

	  private Looper mServiceLooper;
	  private Handler handler;

	  // more methods will go here…
	
	  @Override
	  public void onCreate() {

	    super.onCreate();

	    Log.i(TAG, "1");
	    HandlerThread thread = new HandlerThread("WherewolfThread",
	        Process.THREAD_PRIORITY_BACKGROUND);

	    thread.start();
	    Log.i(TAG, "2");

	    mServiceLooper = thread.getLooper();
	    handler = new Handler(mServiceLooper);
	    Log.i(TAG, "3");
	    locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
	    Log.i(TAG, "rhewldjfhds");
	    PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
	    
	    wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "DoNotSleep");
	    Log.i(TAG, "10");
	    
	    setNight();
	  }

	  @Override
	  public int onStartCommand(Intent intent, int flags, int startId)
	  {
	    return START_STICKY;
	  }

	  public IBinder onBind(Intent intent) {
	    return null;
	  }

	  @Override
	  public void onDestroy() {
	    locationManager.removeUpdates(this);
	  }
	  
	    
	  @Override
	  public void onLocationChanged(Location location) {
		  Log.i(TAG, "onLocationChanged called");
	        if (location != null) {
	        	Log.i(TAG, "hello");
	        	double lat = location.getLatitude();
	        	double lng = location.getLongitude();
	        	
	        	String stringLat = String.valueOf(lat);
	        	String stringLng = String.valueOf(lng);
	        	
	        	int gameID = user.getCurrentGameID();
	        	String stringGameID = String.valueOf(gameID);
	        	String username = user.getPassword();
	        	String password = user.getUsername();
	        	
	            final String locMsg = "location changed "+ lat + " "+ lng;

	            Toast.makeText(getBaseContext(), locMsg, Toast.LENGTH_SHORT).show();
	            //showLocation(locMsg);

	            Log.i(TAG, locMsg);
	            UpdateGameRequest update = new UpdateGameRequest(username, password, stringLat, stringLng, stringGameID);
	            update.execute(new WherewolfNetworking());
	           
	            
//	            net.sendServerUpdate();
//	            
//	            net.sendServerUpdate();
//	            Log.i(TAG, "Network is " + net.isNetworkAvailable(getApplicationContext()));
//
//	            Message msg = mServiceHandler.obtainMessage();
//	            msg.arg1 = ++counter;
//	            mServiceHandler.sendMessage(msg);

	        }
	        
	    }
		public void setNight()
		{
			 Log.i(TAG, "got to setNight");
		  handler.post( new Runnable() {
		     @Override
		      public void run ()
		      {
		    	  Log.i(TAG, "got here");
		          wakeLock.acquire();

		          // makes location updates happen every 5 seconds, with no minimum 
		          // distance for change notification            
		          locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
		              REPORT_PERIOD, MIN_CHANGE, WherewolfService.this);
		          isNight = true;
		      }
		  });
		  
		}

		public void setDay() {

			Log.i(TAG, "4");
		      handler.post(new Runnable() {
		          @Override
		          public void run() {
		              // Log.i(TAG, "Setting to day, turning off tracking");

		              if (isNight) {

		                  if (wakeLock.isHeld()) {
		                      wakeLock.release();
		                  }

		                  locationManager.removeUpdates(WherewolfService.this);

		                  isNight = false;
		                  Log.i(TAG, "Setting to day, turning off tracking");
		                  
		              }
		          }
		      });
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub
			
		}
}

		  
	   
