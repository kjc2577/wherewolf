package edu.utexas.chow.wherewolf;

public class GameSelectionResponse extends BasicResponse {
	protected String status;
	protected String message;
	  private int playerID = -1;

	  public GameSelectionResponse(String status, String errorMessage) {
	      super(status, errorMessage);
	  }
	  
	  public GameSelectionResponse(String status, String errorMessage, int playerID) {
	      super(status, errorMessage);
	      
	      this.playerID = playerID;
	  }

	  
	  public int getPlayerID()
	  {
	      return playerID;
	  }

}
