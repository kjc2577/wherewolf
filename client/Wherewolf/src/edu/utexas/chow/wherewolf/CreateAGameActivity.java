package edu.utexas.chow.wherewolf;


import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class CreateAGameActivity extends Activity {

	private static final String TAG = "creategame";
	public void startLobby()
	{
		Intent intent = new Intent(this, LobbyActivity.class);
		Log.i(TAG, "logging in");
	    startActivity(intent);
	}
	public void lobby(View view){
		
		Toast.makeText(getApplicationContext(), "Redirecting...", 
	    Toast.LENGTH_SHORT).show();
		Log.i(TAG, "calling the logging in function");
		startLobby();	
		      
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_agame);
		final Button button = (Button) findViewById(R.id.create_game_button);

		View.OnClickListener hal = new View.OnClickListener() {
			public void onClick(View v) {
				Log.i(TAG, "starting the login");
				final EditText nameTV = (EditText) findViewById(R.id.game_name);
			    final EditText descriptionTV = (EditText) findViewById(R.id.game_description);
			    
			        
			     String gameName = nameTV.getText().toString();
			     String description = descriptionTV.getText().toString();
			     WherewolfPreferences pref = new WherewolfPreferences(CreateAGameActivity.this);
			     String username = pref.getUsername();
			     String password = pref.getPassword();
			     
			     CreateGameRequest request = new CreateGameRequest(username, password, gameName, description);
			     new CreateGameAsyncTask().execute(request);
				
			}
		};
		button.setOnClickListener(hal);

	}

	private class CreateGameAsyncTask extends AsyncTask<CreateGameRequest, Integer, CreateGameResponse> {
	    @Override
	    protected CreateGameResponse doInBackground(CreateGameRequest... request) {

	    	final EditText nameTV = (EditText) findViewById(R.id.game_name);
		    final EditText descriptionTV = (EditText) findViewById(R.id.game_description);
		    WherewolfPreferences pref = new WherewolfPreferences(CreateAGameActivity.this);
		    String username = pref.getUsername();
		    String password = pref.getPassword();
	        //make a firstname and lastname box
	        
		    String gameName = nameTV.getText().toString();
		    String description = descriptionTV.getText().toString();
		    
	        pref.setNewGame(gameName, description);
		    
	        
	        Log.i(TAG, "hey");
	        CreateGameRequest CreateGameRequest = new CreateGameRequest(username, password , gameName, description);
	        
	        return CreateGameRequest.execute(new WherewolfNetworking());
	    }
	    protected void onPostExecute(CreateGameResponse result) {
	    	Log.i(TAG, "weeeeeeee");
	       
	        
	        if (result.getStatus().equals("success")) {
	            
	            WherewolfPreferences pref = new WherewolfPreferences(CreateAGameActivity.this);
	            pref.setCurrentGameID(result.getGameID());
	            Log.v(TAG, "Signing in");
	            Intent intent = new Intent(CreateAGameActivity.this, LobbyActivity.class);
	            startActivity(intent);
	      //      overridePendingTransition(R.anim.slide_in_right,
	        //            R.anim.slide_out_left);
	        }
	    	}
		}
		
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.create_agame, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
