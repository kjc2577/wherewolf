package edu.utexas.chow.wherewolf;

import java.util.ArrayList;
import java.util.List;


import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;


public class JoinGameRequest extends BasicRequest {
	  private final String TAG = "gameselection";
	  private final String username;
	  private final String password;
	  private final int game_id;
	  private int player_id=-1;
	  
	  public JoinGameRequest(String username, String password, int game_id) {
	      
	      super(username, password);
	      
	      this.username = username;
	      this.password = password;
	      this.game_id = game_id;
	      Log.v(TAG, "register request is starting WHATTTUP");
	  }

	  public String getUsername() {
	      return username;
	  }

	  public String getPassword() {
	      return password;
	  }
	  
	  
	  public int getGame_id() {
	      return game_id;
	  }


	  @Override
	  public String getURL() {
	      return "/v1/game/"+ this.game_id + "/lobby";
	  }
	  
	  @Override
	  public RequestType getRequestType()
	  {
	      return RequestType.POST;
	  }

	  @Override
	  public List<NameValuePair> getParameters() {
	      List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
	      urlParameters.add(new BasicNameValuePair("username", username));
	      urlParameters.add(new BasicNameValuePair("game_id", Integer.toString(game_id)));	  
	      urlParameters.add(new BasicNameValuePair("password", password));	 
	      return urlParameters;
	  }
	  
	  public JoinGameResponse processResponse(JSONObject jObject) throws JSONException
	  {
	      
	      JSONObject jResults = jObject.getJSONObject("results");
	      String username = jResults.getString("username");
	      Log.v(TAG, "username processresponse "+username);

	      return new JoinGameResponse("success", "Successfully joined game ", game_id, player_id);
	  }

	  @Override
	  public JoinGameResponse execute(WherewolfNetworking net) {
	      
	      try {
	          JSONObject jObject = net.sendRequest(this);
	          String status = jObject.getString("status");
	          
	          if (status.equals("success"))
	          {
		          int playerID = jObject.getInt("player_id");
			      player_id = playerID;
			      
			      Log.v(TAG, username + " has playerid "+ player_id);
			      
	              return new JoinGameResponse("success", username + " joined game ", game_id, player_id);
	          } else {
	              String errorMessage = jObject.getString("error");
	              return new JoinGameResponse("failure", errorMessage);
	          }
	          
	      } catch (WherewolfNetworkException ex)
	      {
	          return new JoinGameResponse("failure", "could not communicate with server.");
	      } catch (JSONException e) {
	          return new JoinGameResponse("failure", "could not parse JSON.");
	      }
	      
	  }
}
	  
	