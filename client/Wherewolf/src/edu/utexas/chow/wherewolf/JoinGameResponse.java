package edu.utexas.chow.wherewolf;
import android.util.Log;

public class JoinGameResponse extends BasicResponse{
	private final String TAG = "register response";
	protected int player_id = -1;
	protected int game_id = -1;
	
	public JoinGameResponse(String status, String message){
		super(status, message);
	}
	
	public JoinGameResponse(String status, String message, int player_id, int game_id){
		super(status, message);
		
		this.player_id = player_id;
		this.game_id = game_id;
		
	    Log.v(TAG, "player_id "+this.player_id + "and game_id " + this.game_id);
	}
	
	public int getPlayer_Id()
	{
	    return player_id;
	}
	
	public int getGameID()
	{
	    return game_id;
	}

}